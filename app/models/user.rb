class User < ApplicationRecord
  has_many :additional_infos, dependent: :destroy
  accepts_nested_attributes_for :additional_infos, allow_destroy: true
  validates_uniqueness_of :email
  validates :first_name, :last_name, :telephone, :email, :address, presence: true
end
