class AdditionalInfosController < ApplicationController
  before_action :set_additional_info, only: [:show, :update, :destroy]

  # GET /additional_infos
  def index
    @additional_infos = AdditionalInfo.all

    render json: @additional_infos
  end

  # GET /additional_infos/1
  def show
    render json: @additional_info
  end

  # POST /additional_infos
  def create
    @additional_info = AdditionalInfo.new(additional_info_params)

    if @additional_info.save
      render json: @additional_info, status: :created, location: @additional_info
    else
      render json: @additional_info.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /additional_infos/1
  def update
    if @additional_info.update(additional_info_params)
      render json: @additional_info
    else
      render json: @additional_info.errors, status: :unprocessable_entity
    end
  end

  # DELETE /additional_infos/1
  def destroy
    @additional_info.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_additional_info
      @additional_info = AdditionalInfo.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def additional_info_params
      params.require(:additional_info).permit(:art, :cinema, :music, :user_id)
    end
end
