# README


* Primero se crea el endpoint para la informacion del usuario.
* después se crea el endpoint de información adicional, para guardarla en una segunda tabla y generar una relación de uno a muchos.

* Ruby version 2.6.3

* Database creation in cleardb
    ```username: b7ae5784491e51
    password: 7f57ad8d
    host: us-cdbr-gcp-east-01.cleardb.net
    database: gcp_0663218773013cfa2371```

* Endpoint usuario ```http://localhost:3000/users/```
* Endpoint Informacion adicional ```http://localhost:3000/users/:user_id/additional_infos``
* Peticiones REST
```         
                              Prefix Verb   URI Pattern                                                                              Controller#Action
               user_additional_infos GET    /users/:user_id/additional_infos(.:format)                                               additional_infos#index
                                     POST   /users/:user_id/additional_infos(.:format)                                               additional_infos#create
                user_additional_info GET    /users/:user_id/additional_infos/:id(.:format)                                           additional_infos#show
                                     PATCH  /users/:user_id/additional_infos/:id(.:format)                                           additional_infos#update
                                     PUT    /users/`:user_id`/additional_infos/:id(.:format)                                           additional_infos#update
                                     DELETE /users/:user_id/additional_infos/:id(.:format)                                           additional_infos#destroy
                               users GET    /users(.:format)                                                                         users#index
                                     POST   /users(.:format)                                                                         users#create
                                user GET    /users/:id(.:format)                                                                     users#show
                                     PATCH  /users/:id(.:format)                                                                     users#update
                                     PUT    /users/:id(.:format)                                                                     users#update
                                     DELETE /users/:id(.:format)                                                                     users#destroy
 
```