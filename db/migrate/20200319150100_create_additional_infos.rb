class CreateAdditionalInfos < ActiveRecord::Migration[6.0]
  def change
    create_table :additional_infos do |t|
      t.string :art
      t.string :cinema
      t.string :music
      t.references :user, null: false, foreign_key: true

      t.timestamps
    end
  end
end
