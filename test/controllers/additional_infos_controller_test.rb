require 'test_helper'

class AdditionalInfosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @additional_info = additional_infos(:one)
  end

  test "should get index" do
    get additional_infos_url, as: :json
    assert_response :success
  end

  test "should create additional_info" do
    assert_difference('AdditionalInfo.count') do
      post additional_infos_url, params: { additional_info: { art: @additional_info.art, cinema: @additional_info.cinema, music: @additional_info.music, user_id: @additional_info.user_id } }, as: :json
    end

    assert_response 201
  end

  test "should show additional_info" do
    get additional_info_url(@additional_info), as: :json
    assert_response :success
  end

  test "should update additional_info" do
    patch additional_info_url(@additional_info), params: { additional_info: { art: @additional_info.art, cinema: @additional_info.cinema, music: @additional_info.music, user_id: @additional_info.user_id } }, as: :json
    assert_response 200
  end

  test "should destroy additional_info" do
    assert_difference('AdditionalInfo.count', -1) do
      delete additional_info_url(@additional_info), as: :json
    end

    assert_response 204
  end
end
